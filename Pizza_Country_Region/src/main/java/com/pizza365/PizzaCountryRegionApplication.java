package com.pizza365;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PizzaCountryRegionApplication {

	public static void main(String[] args) {
		SpringApplication.run(PizzaCountryRegionApplication.class, args);
	}

}
